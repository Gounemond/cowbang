﻿using UnityEngine;
using System.Collections;

public class PlayVideo : MonoBehaviour 
{
	MovieTexture movie;
	public string nextLevel;
	public int timeToStart = 0;
	bool isPlaying = false;
	
	// Use this for initialization
	void Start () 
	{
		// Simple as eating bread! Get the movietexture from the renderer
		// and simply Play()
		movie = GetComponent<Renderer>().material.mainTexture as MovieTexture;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Time.time >= timeToStart && !isPlaying)
		{
			GetComponent<MeshRenderer>().enabled = true;
			movie.Play ();
			isPlaying = true;
		}
		if (Time.time >= timeToStart +7.3f && isPlaying)
		{
			movie.Stop ();
		}
		// If the user press a key to skip the video, or the video finishes, switch to next scene
		if (Time.time > (movie.duration + timeToStart +2) || Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.Escape) || Input.GetKey(KeyCode.Space))
		{
			Application.LoadLevel (nextLevel);
		}	
	}
}