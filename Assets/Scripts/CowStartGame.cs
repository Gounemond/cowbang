﻿using UnityEngine;
using System.Collections;

public class CowStartGame : MonoBehaviour {

	public SpriteRenderer text;
	private bool hasBeenClicked = false;
	private bool isMouseHover = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnMouseEnter()
	{
		if (GameState.numberPlayers > 0)
		{
			GetComponent<SpriteRenderer>().enabled = true;
			text.enabled = true;
			isMouseHover = true;
		}
	}
	
	void OnMouseDown ()
	{
		if (!hasBeenClicked && GameState.numberPlayers > 0)
		{
			hasBeenClicked = true;
			Application.LoadLevel (2);
		}
		else
		{
			hasBeenClicked = false;
		}
	}
	
	void OnMouseExit()
	{
		isMouseHover = false;
		if (!hasBeenClicked)
		{
			GetComponent<SpriteRenderer>().enabled = false;
			text.enabled = false;
		}
	}
}
