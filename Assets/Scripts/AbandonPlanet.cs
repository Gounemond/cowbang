﻿using UnityEngine;
using System.Collections;

public class AbandonPlanet : MonoBehaviour {


	public SpriteRenderer glowText;
	private bool hasBeenClicked = false;
	private bool isMouseHover = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnMouseEnter()
	{
		glowText.enabled = true;
		isMouseHover = true;
	}
	
	void OnMouseDown ()
	{
		if (!hasBeenClicked)
		{
			hasBeenClicked = true;
			Application.Quit ();
		}
		else
		{
			hasBeenClicked = false;
		}
	}
	
	void OnMouseExit()
	{
		isMouseHover = false;
		glowText.enabled = false;
	}
}
