﻿using UnityEngine;
using System.Collections;

public class SelectPlayer : MonoBehaviour 
{
	public int playerToSelect;
	public SpriteRenderer text;
	public int playerColor;
	private bool hasBeenClicked = false;
	private bool isMouseHover = false;
	
	private Color color;
	// Use this for initialization
	void Start () 
	{
		GetComponent<SpriteRenderer>().enabled = true;
		color = GetComponent<SpriteRenderer>().color;		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!isMouseHover && !hasBeenClicked)
		{
			GetComponent<SpriteRenderer>().color = new Color(color.r,color.g,color.b, Mathf.Abs(Mathf.Sin(Time.timeSinceLevelLoad + playerToSelect*90))-0.4f);
		}
	}
	
	void OnMouseEnter()
	{
		GetComponent<SpriteRenderer>().color = new Color(color.r,color.g,color.b,1f);
		text.enabled = true;
		isMouseHover = true;
	}
	
	void OnMouseDown ()
	{
		if (!hasBeenClicked)
		{
			hasBeenClicked = true;
			GameState.numberPlayers++;
			GameState.whichPlayerSelected = playerColor;
			text.GetComponent<Shake>().shake = 0.3f;
		}
		else
		{
			hasBeenClicked = false;
			GameState.numberPlayers--;
			text.enabled = false;
			if (playerColor == 0 && GameState.numberPlayers == 1)
			{
				GameState.whichPlayerSelected = 1;
			}
			else if (playerColor == 1 && GameState.numberPlayers == 1)
			{
				GameState.whichPlayerSelected = 0;
			}
		}
	}
	
	void OnMouseExit()
	{
		isMouseHover = false;
		if (!hasBeenClicked)
		{
			text.enabled = false;
		}
	}
}
