﻿using UnityEngine;
using System.Collections;

public class EndingVideo : MonoBehaviour 
{
	public MovieTexture movie;
	public string nextLevel;
	public int timeToStart = 0;
	bool isPlaying = false;
	
	// Use this for initialization
	void Start () 
	{
		// Simple as eating bread! Get the movietexture from the renderer
		// and simply Play()
		//movie = GetComponent<Renderer>().material.mainTexture as MovieTexture;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Time.timeSinceLevelLoad >= timeToStart && !isPlaying)
		{
			GetComponent<MeshRenderer>().enabled = true;
			movie.Play ();
			isPlaying = true;
		}
		if (Time.timeSinceLevelLoad >= timeToStart + movie.duration && isPlaying)
		{
			movie.Stop ();
		}
		// If the user press a key to skip the video, or the video finishes, switch to next scene
		if (Time.timeSinceLevelLoad > (movie.duration + timeToStart +2))
		{
			Application.LoadLevel (nextLevel);
		}	
	}
}