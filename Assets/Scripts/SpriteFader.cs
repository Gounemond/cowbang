﻿using UnityEngine;
using System.Collections;

public class SpriteFader : MonoBehaviour 
{

	public float minimum = 0.0f;
	public float maximum = 1f;
	public float duration = 5.0f;
	private float startTime;
	private SpriteRenderer sprite;
	
	void Awake()
	{
		sprite = GetComponent<SpriteRenderer>();
	}
		
	void Update() 
	{
		float t = Time.time / duration;
		sprite.color = new Color(1f,1f,1f,Mathf.SmoothStep(minimum, maximum, t));        
	}
}
