﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameState : MonoBehaviour {

	public static int numberPlayers = 0;
	public static int whichPlayerSelected = 0;
	public static List<CowHandler> cows;
	public static bool gameStarted = false;
	
	private bool increment1 = false;
	private bool increment2 = false;
	private bool increment3 = false;
	private bool increment4 = false;
	private bool increment5 = false;
	private bool increment6 = false;
	
	
	public static List<float> milkPlayerAmount;
	
	// Use this for initialization
	void Awake () 
	{
		cows = new List<CowHandler>();
		milkPlayerAmount = new List<float>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (gameStarted)
		{
			for (int i = 0; i < milkPlayerAmount.Count; i++)
			{
				GameObject.Find ("MilkAmountPl" + (i+1)).GetComponent<Text>().text = milkPlayerAmount[i].ToString ();
			}
			
			GameObject.Find ("Timer").GetComponent<Text>().text = Time.time.ToString();
			
			if (Time.timeSinceLevelLoad > 20 && !increment1)
			{
				for (int i = 0; i < cows.Count; i++)
				{
					cows[i].speedMilkProduction += Random.Range (1,3);
				}
				increment1 = true;
			}
			
			if (Time.timeSinceLevelLoad > 40 && !increment2)
			{
				for (int i = 0; i < cows.Count; i++)
				{
					cows[i].speedMilkProduction += Random.Range (1,3);
				}
				increment2 = true;
			}
			
			if (Time.timeSinceLevelLoad > 60 && !increment3)
			{
				for (int i = 0; i < cows.Count; i++)
				{
					cows[i].speedMilkProduction += Random.Range (2,5);
				}
				increment3 = true;
			}
			
			if (Time.timeSinceLevelLoad > 80 && !increment4)
			{
				for (int i = 0; i < cows.Count; i++)
				{
					cows[i].speedMilkProduction += Random.Range (5,10);
				}
				increment4 = true;
			}
			
			if (Time.timeSinceLevelLoad > 100 && !increment5)
			{
				for (int i = 0; i < cows.Count; i++)
				{
					cows[i].speedMilkProduction += Random.Range (35,50);
				}
				increment5 = true;
			}
			
			if (Time.timeSinceLevelLoad > 120 && !increment6)
			{
				for (int i = 0; i < cows.Count; i++)
				{
					cows[i].speedMilkProduction += Random.Range (2,5);
				}
				increment6 = true;
			}
			
			if (cows.Count == 0)
			{
				Application.LoadLevel (3);
			}	
			
		}
		
	}
}
