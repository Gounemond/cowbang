﻿using UnityEngine;
using System.Collections;

public class CowHandler : MonoBehaviour 
{
	const float MAX_MILK = 200;				// Maximum liters of milk before the cow explodes
	
	public Sprite satisfiedCow1;
	public Sprite satisfiedCow2;
	public Sprite normalCow1;
	public Sprite normalCow2;
	public Sprite fullCow1;
	public Sprite fullCow2;
	public Sprite fullCow3;
	public Sprite fullCow4;
	public Sprite explodingCow;
	
	public AudioClip satisfiedMuggito;
	public AudioClip normalMuggito;
	public AudioClip overloadedMuggito;
	public AudioClip explodingMuggito;
	
	public int milkStep = 0;
	
	public float speedMilkProduction = 2;
	public float speedMilking = 10;
	
	public ParticleSystem explosion;
	public ParticleSystem ring;
	
	public bool isMilking = false;
	public float milkAmount = 0;
	
	private int milkingPlayerNumber = 0;
	
	
	
	
	// Use this for initialization
	void Start () 
	{
		isMilking = false;
		GameState.cows.Add (this);
	}
	
	// Update is called once per frame
	void Update () 
	{
		// Accumulating Milk
		milkAmount += speedMilkProduction * Time.deltaTime;
		
		// If some Yip is milking
		if (isMilking && milkAmount > MAX_MILK/10)
		{
			milkAmount -= speedMilking *Time.deltaTime;
			GameState.milkPlayerAmount[milkingPlayerNumber] += speedMilking *Time.deltaTime;
		}
								
		if (milkAmount >= 0 && milkAmount <= MAX_MILK/10 && milkStep != 1)
		{
			GetComponent<SpriteRenderer>().sprite = satisfiedCow1;
			GetComponent<AudioSource>().PlayOneShot (satisfiedMuggito);
			milkStep = 1;
		}
		if (milkAmount >= MAX_MILK/10 && milkAmount <= MAX_MILK/4 && milkStep != 2)
		{
			GetComponent<SpriteRenderer>().sprite = satisfiedCow2;
			milkStep = 2;
		}
		
		if (milkAmount > MAX_MILK/4 && milkAmount <= 2*MAX_MILK/4 && milkStep != 3)
		{
			GetComponent<SpriteRenderer>().sprite = normalCow1;
			GetComponent<AudioSource>().PlayOneShot (normalMuggito);
			milkStep = 3;
		}
		if (milkAmount > 2*MAX_MILK/4 && milkAmount <= 8*MAX_MILK/12 && milkStep != 4)
		{
			GetComponent<SpriteRenderer>().sprite = normalCow2;
			milkStep = 4;
		}
		
		if (milkAmount > 8*MAX_MILK/12 && milkAmount <= 9*MAX_MILK/12 && milkStep != 5)
		{
			GetComponent<SpriteRenderer>().sprite = fullCow1;
			GetComponent<AudioSource>().PlayOneShot (overloadedMuggito);
			milkStep = 5;
		}
		if (milkAmount > 9*MAX_MILK/12 && milkAmount <= 10*MAX_MILK/12 && milkStep != 6)
		{
			GetComponent<SpriteRenderer>().sprite = fullCow2;
			milkStep = 6;
		}
		if (milkAmount > 10*MAX_MILK/12 && milkAmount <= 11*MAX_MILK/12 && milkStep != 7)
		{
			GetComponent<SpriteRenderer>().sprite = fullCow3;
			milkStep = 7;
		}
		if (milkAmount > 11*MAX_MILK/12 && milkAmount <= MAX_MILK-5 && milkStep != 8)
		{
			GetComponent<SpriteRenderer>().sprite = fullCow4;
			milkStep = 8;
		}
		if (milkAmount > MAX_MILK-5 && milkAmount < MAX_MILK && milkStep != 9)
		{
			GetComponent<SpriteRenderer>().sprite = explodingCow;
			GetComponent<AudioSource>().PlayOneShot(explodingMuggito);
			milkStep = 9;
		}
		
		if (milkAmount >= MAX_MILK)
		{
			StartCoroutine (ExplodeAndDie ());
		}
		
		
		
	
	}
	
	public IEnumerator ExplodeAndDie()
	{
		explosion.Play ();
		ring.Play ();
		GetComponent<SpriteRenderer>().enabled = false;
		yield return new WaitForSeconds(explosion.duration);
		GameState.cows.Remove (this);
		Destroy (this.gameObject);
		
	}
	
	void OnCollisionEnter2D(Collision2D coll) 
	{
		if (coll.gameObject.tag == "Player")
		{
			milkingPlayerNumber = coll.gameObject.GetComponent<YipController>().yipNumber;
			isMilking = true;
		}
	}
	
	void OnCollisionExit2D(Collision2D coll) 
	{
		if (coll.gameObject.tag == "Player")
		{
			isMilking = false;
		}
	}
}
