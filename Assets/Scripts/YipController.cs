﻿using UnityEngine;
using System.Collections;

public class YipController : MonoBehaviour 
{

	public string horizontalMovement;
	public string verticalMovement;
	public int yipNumber = 0;
	
	public ParticleSystem milkingEffect;

	// Use this for initialization
	void Start () 
	{
		GameState.milkPlayerAmount.Add (0);
		GameState.gameStarted = true;
		if (yipNumber == 0 && GameState.numberPlayers == 1 && GameState.whichPlayerSelected == 1)
		{
			Destroy (this.gameObject);
		}
		if (yipNumber == 1 && GameState.numberPlayers == 1 && GameState.whichPlayerSelected == 0)
		{
			Destroy (this.gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
	
		if (Input.GetAxis (horizontalMovement) != 0)
		{
			this.transform.position = new Vector3 (this.transform.position.x + Input.GetAxis (horizontalMovement)*Time.deltaTime, this.transform.position.y,this.transform.position.z);
		}
		
		if (Input.GetAxis (verticalMovement) != 0)
		{
			this.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y +Input.GetAxis (verticalMovement)*Time.deltaTime,this.transform.position.z);
		}
	
	}
	
	void OnCollisionEnter2D(Collision2D coll) 
	{
		if (coll.gameObject.tag == "Cow")
		{
			milkingEffect.Play ();
			GetComponent<AudioSource>().Play ();
		}
	}
	
	void OnCollisionExit2D(Collision2D coll) 
	{
		if (coll.gameObject.tag == "Cow")
		{
			milkingEffect.Stop ();
			GetComponent<AudioSource>().Stop ();
		}
	}
}
