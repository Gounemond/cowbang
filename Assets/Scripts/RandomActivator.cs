﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RandomActivator : MonoBehaviour 
{
	public GameObject[] endings;
	public AudioClip[] clips;
	
	public Text redmilk;
	public Text yellowmilk;
	public Image redTrophy;
	public Image yellowTrophy;

	// Use this for initialization
	void Start () 
	{
		int choice = Random.Range (0,endings.Length);
		endings[choice].SetActive(true);
		GetComponent<AudioSource>().clip = clips[choice];
		GetComponent<AudioSource>().Play ();
		if (GameState.numberPlayers == 1 && GameState.whichPlayerSelected == 1)
		{
			yellowmilk.text = GameState.milkPlayerAmount[0].ToString ();
			redmilk.text = "0";
		}
		else if (GameState.numberPlayers == 0 && GameState.whichPlayerSelected == 0)
		{
			redmilk.text = GameState.milkPlayerAmount[0].ToString ();
			yellowmilk.text = "0";
		}
		else
		{
			redmilk.text = GameState.milkPlayerAmount[0].ToString ();
			yellowmilk.text = GameState.milkPlayerAmount[1].ToString ();
			if (GameState.milkPlayerAmount[0] > GameState.milkPlayerAmount[1])
			{
				redTrophy.enabled = true;
			}
			else if (GameState.milkPlayerAmount[0] < GameState.milkPlayerAmount[1])
			{
				yellowTrophy.enabled = true;
			}			
			else
			{
				redTrophy.enabled = true;
				yellowTrophy.enabled = true;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
